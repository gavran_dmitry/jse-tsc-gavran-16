package ru.tsc.gavran.tm.exception.entity;

import ru.tsc.gavran.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }
}
